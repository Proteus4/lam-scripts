**Scripts**

```
Name: seido.sh
Location: seido-install-scripts
Description: Software to Install on a freshly installed Linux system
``` 

```
Name: autopull.sh
Location: seido-install-scripts
Description: Auto pulls and syncs github repository
```

```
Name: forge-server-1.16.3-34.1.35.sh
Location: proteus-install-scripts/misc scripts
Description: Auto downloads and installs forge
```

| Title | File Location | Summary | Quick Use |
| ------ | ------ | ------ | ------ |
| **Seido Install** | [/seido-install-scripts/](/seido-install-scripts/) | Software to Install on a freshly installed Linux system | `wget https://gitlab.com/Proteus4/lam-scripts/-/raw/master/seido-install-scripts/seido.sh \| bash` |
| **GitHub Autopull** | [/seido-install-scripts/](/seido-install-scripts/) | Auto pulls and syncs github repository | `wget https://gitlab.com/Proteus4/lam-scripts/-/raw/master/seido-install-scripts/autopull.sh \| bash` |
| **Forge 1.16.3 Install** | [/proteus-install-scripts/misc scripts/](/proteus-install-scripts/misc scripts/) | Auto downloads and installs forge | `wget https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/misc%20scripts/forge-server-1.16.3-34.1.35.sh \| bash` |
| **Mullvad Install** | [/taghead-linux-scripts/](/taghead-linux-scripts/) | Installs Mullvad Client from github | `wget https://gitlab.com/Proteus4/lam-scripts/-/raw/master/taghead-linux-scripts/Install-Mullvad.sh \| bash` |
| **ZSH (w/Oh-My-ZSH) Install** | [/taghead-linux-scripts/](/taghead-linux-scripts/) | Installs ZSH from repository and Oh-My-ZSH from github| `wget https://gitlab.com/Proteus4/lam-scripts/-/raw/master/taghead-linux-scripts/Install-ZSH.sh \| bash` |

**Directories**
```
Seido - seido-install-scripts
Proteus - proteus-install-scripts
Taghead - taghead-linux-scripts
```
