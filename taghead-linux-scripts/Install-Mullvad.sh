#!/bin/sh

#                               OVERVIEW
# - Tested on Debian Buster
# - Obtains latest version of Mullvad from https://github.com/mullvad/mullvadvpn-app/releases/
# - Installs dependencies 

#Variables change                                     #     SEARCHABLE TERMS        -                    DESCRIPTION
InstallBasePackages=( 'curl' 'sudo' 'jq' )            #  Install Base Packages      -  an array of packages passed into apt-get install

## Install Base Packages and Install Preferred Packages
sudo apt-get install -y --assume-yes -q ${InstallBasePackages[*]}

# Install Mullvad
sudo wget "$(curl -s https://api.github.com/repos/mullvad/mullvadvpn-app/releases/latest | jq -r ".assets[].browser_download_url" | grep amd64.deb$)" -O '/tmp/mullvad.deb'
sudo dpkg -i /tmp/mullvad.deb
sudo apt-get install -y --assume-yes -q -f