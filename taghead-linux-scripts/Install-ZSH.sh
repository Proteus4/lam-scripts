#!/bin/sh

#                               OVERVIEW
# - Tested on RaspberryPi 4B using DietPi 
# - Installs ZSH from available repositories through apt
# - Installs Base Packages 
# - Obtains latest version of Oh-My-ZSH from https://github.com/robbyrussell/oh-my-zsh.git
# - Obtains latest version of zsh-autosuggestions from https://github.com/zsh-users/zsh-autosuggestions

#Variables change
InstallBasePackages=( 'git' 'wget' 'zsh' )

## Install Base Packages
sudo apt-get install -y --assume-yes -q ${InstallBasePackages[*]}

## Obtains latest version of Oh-My-ZSH from https://github.com/robbyrussell/oh-my-zsh.git
ZSH="/opt/oh-my-zsh" sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

## Obtains latest version of zsh-autosuggestions from https://github.com/zsh-users/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

## Add to ZSH Plu
sed -i '/^source.*/i plugins=(zsh-autosuggestions)' $HOME/.zshrc

echo "Run \"chsh -s /bin/zsh\" to change default shell"

echo "Edit both \"/etc/adduser.conf\" and \"/etc/default/useradd\" from \"DSHELL=/bin/sh\" to \"DSHELL=/bin/zsh\" to change default shell for new user"

echo "Change the ZSH theme by editing the file \".zshrc\" in the home directory"