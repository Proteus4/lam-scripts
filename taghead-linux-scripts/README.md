# Script Details 

| Title | File Location | Summary | Test Environment |
| ------ | ------ | ------ | ------ |
| **Mullvad Install Script** | [/taghead-linux-scripts/Install-Mullvad.sh](/taghead-linux-scripts/Install-Mullvad.sh) | Installs Mullvad Client from github | Debian 9 |
| **ZSH (w/Oh-My-ZSH) Install Script** | [/taghead-linux-scripts/Install-ZSH.sh](/taghead-linux-scripts/Install-ZSH.sh) | Installs ZSH from repository and Oh-My-ZSH from github| RaspberryPi 4B using DietPi |


