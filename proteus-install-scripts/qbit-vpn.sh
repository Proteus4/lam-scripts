sudo pacman -Syyu --noconfirm

sudo pacman -S wireguard-tools resolvconf qbittorrent-nox samba cifs-utils --noconfirm

CONFIG_FILE = "/etc/wireguard/au-mel-wg-301.conf"

check_config_file() {
    if [ ! -f "$CONFIG_FILE" ]; then
        echo "The configuration file $CONFIG_FILE does not exist."
        echo "Please copy the server au-mel-wg-301 file to /etc/wireguard/."
        echo "Press any key to continue..."
        read -n 1 -s  # Wait for the user to press any key
        echo # Print a newline for better readability
    fi
}

# Loop until the configuration file exists
while true; do
    check_config_file
    if [ -f "$CONFIG_FILE" ]; then
        echo "Configuration file found. Proceeding..."
        break  # Exit the loop if the file exists
    fi
done

#Wireguard service
sudo chmod 600 /etc/wireguard/au-mel-wg-301.conf
sudo systemctl enable wg-quick@au-mel-wg-301
sudo systemctl start wg-quick@au-mel-wg-301
curl https://api.ipify.org

# Allow local traffic, force outside traffic through vpn only
sudo iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A OUTPUT -d 192.168.1.0/24 -j ACCEPT
sudo iptables -A INPUT -s 192.168.1.0/24 -j ACCEPT
sudo iptables -A OUTPUT -o au-mel-wg-301 -j ACCEPT
sudo iptables -A INPUT -i au-mel-wg-301 -j ACCEPT
sudo iptables-save | sudo tee /etc/iptables/iptables.rules

#QBit service
sudo wget -O \etc\systemd\system\qbittorrent-nox.service https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/qbittorrent-nox.service
sudo systemctl daemon-reload
sudo systemctl enable qbittorrent-nox
sudo systemctl start qbittorrent-nox

# Samba Service
sudo mkdir -p /mnt/sambashare
sudo echo "username=pi" | sudo tee -a /etc/samba/credentials
# Read the password
read -s -p "Enter the password: " password
echo
# Add the password to the credentials file
echo "password=$password" | sudo tee -a /etc/samba/credentials
sudo chmod 600 /etc/samba/credentials

echo "//raspberrypi/zfspool/downloads /mnt/sambashare cifs credentials=/etc/samba/credentials,uid=1000,gid=1000,file_mode=0777,dir_mode=0777,nofail 0 0" | sudo tee -a /etc/fstab
sudo mount -a

#Drops SSH connection
sudo iptables -A OUTPUT -j DROP && sudo service iptables save