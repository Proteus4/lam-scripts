@ECHO OFF
REM https://bit.ly/3h3TRV0
WINGET INSTALL Brave.Brave.Beta
WINGET INSTALL Valve.steam
WINGET INSTALL Malwarebytes.Malwarebytes
WINGET INSTALL Nvidia.GeforceExperience
WINGET INSTALL Resilio.ResilioSync
WINGET INSTALL Discord.Discord
WINGET INSTALL PeterPawlowski.foobar2000
WINGET INSTALL tailscale.tailscale
WINGET INSTALL Audacity.Audacity
WINGET INSTALL Plex.Plex
WINGET INSTALL RARLab.WinRAR
WINGET INSTALL WMware.WorkstationPro
WINGET INSTALL Oracle.JDK.17
WINGET INSTALL Oracle.JavaRuntimeEnvironment
