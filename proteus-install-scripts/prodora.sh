#https://bit.ly/3qAjzlw
#!/bin/bash
while :; do
	clear
	echo ""
	echo ""
	echo "THIS SCRIPT IS FOR FEDORA-BASED DISTRIBUTIONS."
	echo "Please don't try to run this on Arch or Debian"
	echo ""
	echo "Please select an option"
	echo "1) Remove unwanted applications"
	echo "2) Automated package install"
	echo "3) Install Resilio Sync (Optional)"
	echo "99) Exit"

	read INPUT_STRING
	case $INPUT_STRING in

	1)

		sudo dnf remove -y firefox;
		sudo dnf remove -y firefox-locale-en;
		sudo dnf remove -y xfburn;
		sudo dnf remove -y claws-mail;
		sudo dnf remove -y pidgin;
		sudo dnf remove -y asunder;
		sudo dnf remove -y parole;
		sudo dnf remove -y pragha;
		sudo dnf remove -y atril;
		sudo dnf remove -y gnumeric;
		sudo dnf remove -y gparted;
		sudo dnf remove -y geany;
		sudo dnf remove -y xarchiver;
		sudo dnf clean packages -y;
		;;

	2)
		xset s off
		sudo dnf remove -y firefox;
		sudo dnf remove -y firefox-locale-en;
		sudo dnf remove -y xfburn;
		sudo dnf remove -y claws-mail;
		sudo dnf remove -y pidgin;
		sudo dnf remove -y asunder;
		sudo dnf remove -y parole;
		sudo dnf remove -y pragha;
		sudo dnf remove -y atril;
		sudo dnf remove -y gnumeric;
		sudo dnf clean packages -y;
		sudo wget --content-disposition https://bit.ly/36rPLkm -O /etc/dnf/dnf.conf;
		
		sudo dnf update -y;

		#Gnome Disk Utility;
		sudo dnf install -y gnome-disk-utility;


		#Brave Preferences
        mkdir -p ~/.config/BraveSoftware/Brave-Browser-Beta/Default
        wget -O ~/.config/BraveSoftware/Brave-Browser-Beta/Default/Preferences https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/Preferences        

		#brave browser beta
		sudo dnf install dnf-plugins-core;
		sudo dnf config-manager addrepo --from-repofile=https://brave-browser-rpm-beta.s3.brave.com/brave-browser-beta.repo
		sudo dnf install -y brave-browser-beta;

		#discord
		sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm;
		sudo dnf update -y;
		sudo dnf install -y discord;
		
		#mpv
		sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm;
		sudo dnf update -y;
		sudo dnf install -y mpv;

        #Audacity
        sudo dnf install -y audacity

		#neofetch
		sudo dnf install -y neofetch;

		#dislocker
		sudo dnf install -y dislocker
		sudo dnf install -y fuse-dislocker
		sudo dnf install -y openssl
		sudo dnf install -y dnf install dislocker fuse-dislocker
		sudo dnf install -y gcc cmake make fuse-devel mbedtls-devel ruby-devel rubypick
		sudo dnf install -y git
		cd ~
		git clone https://github.com/Aorimn/dislocker.git
		cd dislocker
		cmake .
		make
		sudo make install
		cd ~

		#ffmpeg
		sudo dnf install -y ffmpeg-free;

		#VSCodium
		printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=download.vscodium.com\nbaseurl=https://download.vscodium.com/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg\nmetadata_expire=1h\n" | sudo tee -a /etc/yum.repos.d/vscodium.repo
		sudo dnf install -y codium
		echo 'export XDG_DATA_DIRS="/var/lib/flatpak/exports/share:/home/proteus/.local/share/flatpak/exports/share:$XDG_DATA_DIRS"' >> ~/.zshrc;
		source .zshrc;
		flatpak install -y flathub com.parsecgaming.parsec

		#Parsec
		sudo dnf install -y flatpak
		
		#audacity
		sudo dnf install -y audacity;

		#Tailscale
		sudo dnf config-manager addrepo --from-repofile=https://pkgs.tailscale.com/stable/fedora/tailscale.repo
		sudo dnf install -y tailscale

		#Mullvad
		sudo dnf config-manager addrepo --from-repofile=https://repository.mullvad.net/rpm/stable/mullvad.repo
		sudo dnf install -y mullvad-vpn

		#Gocryptfs
		sudo dnf install -y gocrpyfs

		#steam
		sudo dnf install -y steam;
		
		#Flatpak
		sudo dnf install -y flatpak

		#Redshift
		sudo dnf install -y redshift
		sudo dnf install -y redshift-gtk

		#File-Roller
		sudo dnf install -y file-roller

		sudo dnf install -y xcb-util-cursor
		sudo dnf install -y xcb-util-keysyms
		sudo dnf install -y xcb-util-wm
		sudo dnf install -y libxkbcommon-x11
		export QT_QPA_PLATFORM=offscreen
		
		#Parsec
		source /etc/profile.d/flatpak.sh
		sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
		flatpak update --appstream && flatpak update -y
		flatpak install -y flathub com.parsecgaming.parsec

		#Increase cursor size
		xfconf-query --channel xsettings --property /Gtk/CursorThemeSize --set 41;
		
		#arc-theme
		sudo dnf install -y arc-theme;
		
		#papirus icon theme
		sudo dnf install -y papirus-icon-theme;

		#xfce4 desktop config
		wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/xfce4-desktop.xml
		
		#xfce4 panel config
        wget --content-disposition https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/eos-panel
        xfce4-panel-profiles load eos-panel
        rm eos-panel
		
		#Matcha theme
		sudo dnf install -y gtk-murrine-engine;
		sudo dnf install -y gtk2-engines;
		sudo dnf install -y git;
		git clone https://github.com/vinceliuice/matcha.git;
		cd matcha;
		bash install.sh;
		cd ..;
		sudo rm -r matcha;

		#Keyboard shortcuts
		wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/xfce4-keyboard-shortcuts.xml
		
		#Terminal theme
		wget -O ~/.config/xfce4/terminal/terminalrc https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/terminalrc;
		
		#Thunar Settings
        wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/thunar.xml https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/thunar.xml

		#Window scaling settings
		wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/xsettings.xml

		#Papirus + Matcha Dark Sea config
		xfconf-query -c xfwm4 -p /general/theme -s "Matcha-dark-sea";
		xfconf-query -c xsettings -p /Net/ThemeName -s "Matcha-dark-sea";
		xfconf-query -c xsettings -p /Net/IconThemeName -s "Papirus-Dark";

		#Thermald
		sudo dnf install -y thermald

        #Auto-CPUFreq
        cd ~
        git clone https://github.com/AdnanHodzic/auto-cpufreq.git
        cd auto-cpufreq
        sudo ./auto-cpufreq-installer
		cd ~
		
		#dnf automatic
		sudo dnf install -y dnf-automatic;
		sudo wget -O /etc/dnf/automatic.conf https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/automatic.conf
		sudo systemctl enable --now dnf-automatic.timer;

		#zsh
		wget -O .zshrc https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/.zshrc
		sudo dnf install -y wget
		sudo dnf install -y curl		
		sudo dnf install -y zsh
		sudo dnf install -y powerline-fonts;
        git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
        chsh -s $(which zsh)
        xset s on
		;;

		3)
		#resilio sync
		sudo rpm --import https://linux-packages.resilio.com/resilio-sync/key.asc;
		sudo dnf install -y zypper
		sudo zypper ar --gpgcheck-allow-unsigned-repo -f https://linux-packages.resilio.com/resilio-sync/rpm/\$basearch resilio-sync;
		printf "[resilio-sync]\nname=Resilio Sync\nbaseurl=https://linux-packages.resilio.com/resilio-sync/rpm/\$basearch\nenabled=1\ngpgcheck=1\n" | sudo tee /etc/yum.repos.d/resilio-sync.repo
		sudo dnf update -y;
		sudo dnf install -y resilio-sync;
		sudo service resilio-sync stop;
		sudo systemctl disable resilio-sync;
		wget https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/resilio-sync.service;
		sudo mv resilio-sync.service /usr/lib/systemd/user/;
		systemctl --user enable resilio-sync;
		systemctl --user start resilio-sync;
		;;

	99)
		break;
		;;

	*)
		echo "Invalid input. Try again."
		;;
	esac
done
