#!/bin/bash
#https://bit.ly/3HdcVde
sudo timedatectl set-local-rtc 1
clear
while :; do
echo ""
echo ""
echo "THIS SCRIPT IS FOR ARCH-BASED DISTRIBUTIONS."
echo "Please don't try to run this on Ubuntu."
echo ""
echo "Please select an option"
echo "1)  Remove unwanted applications"
echo "2)  Automated package install"
echo "3)  Install Vmware-Workstation (Optional)"
echo "99) Exit"
echo ""
read INPUT_STRING
case $INPUT_STRING in

    1)
        yay -Rs firefox --noconfirm
        yay -Rs parole --noconfirm
    ;;

    2)
        xset s on
        #Update
        sudo pacman -S archlinux-keyring
        yay -Syyu --noconfirm

        #Belena Etcher
        #yay -S balena-etcher --noconfirm

        #Gnome Disk Utility
        yay -S gnome-disk-utility --noconfirm

        #Steamlink
        #yay -S steamlink --noconfirm

        #Brave Preferences
        mkdir -p ~/.config/BraveSoftware/Brave-Browser-Beta/Default
        wget -O /home/proteus/.config/BraveSoftware/Brave-Browser-Beta/Default/Preferences https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/Preferences        
         
        #Brave Beta
        yay -S brave-beta-bin --noconfirm   

        #Dislocker
        yay -S dislocker --noconfirm

        #MPV
        sudo pacman -S mpv --noconfirm 

        #Audacity
        sudo pacman -S audacity --noconfirm

        #Discord
        sudo pacman -S discord --noconfirm

        #LightDM Settings
        yay -S lightdm-settings --noconfirm

        #XFCE Panel Configuration
        yay -S xfce4-panel-profiles --noconfirm
        wget --content-disposition https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/eos-panel
        xfce4-panel-profiles load eos-panel
        rm eos-panel

        #Papris Icon Theme
        sudo pacman -S papirus-icon-theme --noconfirm
        xfconf-query -c xsettings -p /Net/IconThemeName -s "Papirus-Dark";

        #Matcha Theme
        yay -S matcha-gtk-theme --noconfirm
        xfconf-query -c xfwm4 -p /general/theme -s "Matcha-dark-sea";
        xfconf-query -c xsettings -p /Net/ThemeName -s "Matcha-dark-sea";

        #Terminal Theme
        wget -O ~/.config/xfce4/terminal/terminalrc https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/terminalrc;

        #Keyboard Shortcuts
        wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml         wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/xfce4-keyboard-shortcuts.xml
		
        #Thunar Settings
        wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/thunar.xml https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/thunar.xml

        #Auto-CPUFreq
        cd ~
        git clone https://github.com/AdnanHodzic/auto-cpufreq.git
        cd auto-cpufreq
        sudo ./auto-cpufreq-installer

        #ZSH
        cd ~
        sudo pacman -S zsh --noconfirm
        sudo pacman -S powerline-fonts --noconfirm
        wget -O .zshrc https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/.zshrc
        git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
        chsh -s $(which zsh)
        xset s off
    ;;

    3)
        #Vmware-Workstation
        yay -S vmware-workstation --noconfirm
        sudo modprobe -a vmw_vmci vmmon
        sudo systemctl enable --now vmware-networks
        sudo systemctl enable --now vmware-usbarbitrator
    ;;

    99)
        break
    ;;

    *)
        clear
        echo "Invalid input. Try again."
    ;;
        esac
    done
