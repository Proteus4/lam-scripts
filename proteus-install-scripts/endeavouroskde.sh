#!/bin/bash
#https://tinyurl.com/2ztxwawy

yay -Syyu --noconfirm

sudo pacman -S archlinux-keyring --noconfirm
sudo pacman -S wget --noconfirm

#Debloat
yay -Rs firefox --noconfirm
yay -Rs kate --noconfirm

#Wine
#sudo pacman -S wine --noconfirm
#fwupdmgr get-devices
#https://www.dell.com/support/home/en-au/drivers/driversdetails?driverid=4w96v
# unzip ./FWUpdate_v1.0.3.0_DellMUP_Fv0C_Trial_04_ZPE.exe
# cd FWUpdate_v1.0.3.0_DellMUP_Fv0C_Trial_04
# wine ./FWUpdate.exe
# cp ./TP_Bin.bin ~/
#sudo fwupdtool install-blob \~/TP\_Bin.bin hardware_id

#Intel Hardware Acceleration
sudo pacman -S intel-media-driver --noconfirm

#Intel GPU Top
sudo pacman -S intel-gpu-tools --noconfirm

#Gnome Disk Utility
yay -S gnome-disk-utility --noconfirm

#Brave Preferences
mkdir -p ~/.config/BraveSoftware/Brave-Browser-Beta/Default
wget -O /home/proteus/.config/BraveSoftware/Brave-Browser-Beta/Default/Preferences https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/Preferences

#Brave Beta
yay -S brave-beta-bin --noconfirm

#Dislocker
yay -S dislocker --noconfirm

#Audacity
sudo pacman -S audacity --noconfirm

#Discord
sudo pacman -S discord --noconfirm

#VSCodium
yay -S vscodium-bin --noconfirm

#Thermald
sudo pacman -S thermald --noconfirm
sudo systemctl enable thermald --now

#Parsec
yay -S parsec-bin --noconfirm

#Tailscale
sudo pacman -S tailscale --noconfirm

#Easyeffects
sudo pacman -S easyeffects --noconfirm

#Mullvad
yay -S mullvad-vpn-bin --noconfirm

#Resilio Sync
yay -S rslsync --noconfirm
sudo systemctl stop rslsync.service
sudo systemctl disable rslsync.service
systemctl --user enable rslsync.service
systemctl --user start rslsync.service

#Gocrpyfs
sudo pacman -S gocryptfs --noconfirm

#if ! pacman -Qq | grep -qw linux-cachyos then
#    echo "CachyOS kernel not found. Installing..."
#    
#    # Increase password timeout to 120 minutes
#    sudo sed -i '/^Defaults/ s/$/, timestamp_timeout=120' /etc/sudoers#
#
#    # Install CachyOS kernel and headers
#    yay -S linux-cachyos --noconfirm
#    yay -S linux-cachyos-headers --noconfirm
#    
#    # Regenerate initramfs
#    sudo dracut --regenerate-all --force
#    
#    # Update GRUB configuration
#    sudo grub-mkconfig -o /boot/grub/grub.cfg#
#
#    # Undo password timeout
#    sudo sed -i '/timestamp_timeout=120/d' /etc/sudoers#
#
#    echo "CachyOS kernel installation complete."
#else
#    echo "CachyOS kernel is already installed."
#fi

#EndeavourOS KDE Config
cd ~
yay -S konsave --noconfirm
wget -O endeavouroskde.knsv https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/endeavouroskde.knsv
konsave -i endeavouroskde.knsv
kosnave -a endeavouroskde
rm endeavouroskde.knsv

#Fix scroll speed
touchpad_id=$(xinput list | grep -i touchpad | awk '{print $6}' | sed 's/id=//')
if [ -z "$touchpad_id" ]; then
    echo "No touchpad found."
    continue
fi
sudo xinput set-prop "$touchpad_id" "libinput Scrolling Pixel Distance" 50
echo "Touchpad scroll speed set for ID $touchpad_id."

#Terminal Config
sudo wget -O ~/.local/share/konsole/Profile.profile https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/Profile.profile
sed -i 's/^DefaultProfile=.*$/DefaultProfile=Profile.profile/' ~/.config/konsolerc
sudo chown $USER:$USER ~/.local/share/konsole/Profile.profile

#Plex
sudo pacman -S flatpak --noconfirm
#flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
echo 'export XDG_DATA_DIRS="/var/lib/flatpak/exports/share:/home/proteus/.local/share/flatpak/exports/share:$XDG_DATA_DIRS"' >> ~/.zshrc
source .zshrc
sudo flatpak install flathub tv.plex.PlexDesktop -y

#Auto-CPUFreq
cd ~
git clone https://github.com/AdnanHodzic/auto-cpufreq.git
cd auto-cpufreq
sudo ./auto-cpufreq-installer
cd ~

#ZSH
cd ~
sudo pacman -S neofetch --noconfirm
sudo pacman -S zsh --noconfirm
sudo pacman -S powerline-fonts --noconfirm
wget -O .zshrc https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/.zshrc
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
chsh -s $(which zsh)
