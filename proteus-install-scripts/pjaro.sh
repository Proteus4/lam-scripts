#!/bin/bash
#https://bit.ly/3njvkfv
clear
while :; do

    echo "THIS SCRIPT IS FOR ARCH-BASED DISTRIBUTIONS."
    echo "Please don't try to run this on Ubuntu."
    echo ""
    echo "Please select an option"
    echo "1)  Remove unwanted applications"
    echo "2)  Automated package install"
    echo "3)  Perform options 1 & 2"
    echo "99) Exit"
    echo ""
    read INPUT_STRING
    case $INPUT_STRING in

    1)
    xfconf-query -c xsettings -p /Net/ThemeName -s "Matcha-dark-sea"
    sudo pacman-mirrors --country Australia && sudo pacman -Syyu --noconfirm
    wget --content-disposition https://bit.ly/392EFzK
    sudo cp -prv pamac.conf /etc/
    rm pamac.conf
    sudo pacman -Syu --noconfirm
    sudo pacman -Rs --noconfirm qpdfview
    sudo pacman -Rs --noconfirm audacious
    sudo pacman -Rs --noconfirm firefox
    sudo pacman -Rs --noconfirm gcolor2
    sudo pacman -Rs --noconfirm gimp
    sudo pacman -Rs --noconfirm hexchat
    sudo pacman -Rs --noconfirm xfce4-notes-plugin
    sudo pacman -Rs --noconfirm pidgin
    sudo pacman -Rs --noconfirm steam-manjaro
    sudo pacman -Rs --noconfirm thunderbird
    ;;

    2)
    sudo pacman-mirrors --country Australia && sudo pacman -Syyu
    wget --content-disposition https://bit.ly/392EFzK
    sudo cp -prv pamac.conf /etc/
    rm pamac.conf
    xfconf-query -c xsettings -p /Net/ThemeName -s "Matcha-dark-sea"
    xfconf-query -c xsettings -p /Net/IconThemeName -s "Papirus-Dark-Maia"
    sudo pacman -Syu --noconfirm yay
    sudo pacman -Syu --noconfirm --needed base-devel git
    yay -S --noconfirm google-chrome
    yay -S --noconfirm zoom
    ;;

    3)
    xfconf-query -c xsettings -p /Net/ThemeName -s "Matcha-dark-sea"
    xfconf-query -c xsettings -p /Net/IconThemeName -s "Papirus-Dark-Maia"
    sudo pacman-mirrors --country Australia && sudo pacman -Syyu --noconfirm
    wget --content-disposition https://bit.ly/392EFzK
    sudo cp -prv pamac.conf /etc/
    sudo pacman -Syu --noconfirm
    sudo pacman -Rs --noconfirm qpdfview
    sudo pacman -Rs --noconfirm audacious
    sudo pacman -Rs --noconfirm firefox
    sudo pacman -Rs --noconfirm gcolor2
    sudo pacman -Rs --noconfirm gimp
    sudo pacman -Rs --noconfirm hexchat
    sudo pacman -Rs --noconfirm xfce4-notes-plugin
    sudo pacman -Rs --noconfirm pidgin
    sudo pacman -Rs --noconfirm steam-manjaro
    sudo pacman -Rs --noconfirm thunderbird
    sudo pacman -Syu --noconfirm yay
    sudo pacman -Syu --noconfirm --needed base-devel git
    yay -S --noconfirm google-chrome
    yay -S --noconfirm zoom
    rm pamac.conf
    ;;

    99)
    break
    ;;

    *)
    clear
    echo "Invalid input. Try again."
    ;;
    esac
done
