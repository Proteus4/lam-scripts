#!/bin/bash
#https://bitly.cx/uDtb
sudo dnf install wget -y
sudo wget --content-disposition https://bit.ly/36rPLkm -O /etc/dnf/dnf.conf
sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf update -y
sudo dnf install -y @xfce-desktop-environment
sudo dnf install -y xfce4-whiskermenu-plugin
sudo systemctl set-default graphical.target

#Brave Preferences
mkdir -p ~/.config/BraveSoftware/Brave-Browser-Beta/Default
wget -O ~/.config/BraveSoftware/Brave-Browser-Beta/Default/Preferences https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/Preferences        

#brave browser beta


#neofetch
sudo dnf install -y neofetch

#dislocker
sudo dnf install -y dislocker
sudo dnf install -y fuse-dislocker
sudo dnf install -y openssl
sudo dnf install -y dnf install dislocker fuse-dislocker
sudo dnf install -y gcc cmake make fuse-devel mbedtls-devel ruby-devel rubypick
sudo dnf install -y git
cd ~
git clone https://github.com/Aorimn/dislocker.git
cd dislocker
cmake .
make
sudo make install
cd ~

#ffmpeg
sudo dnf install -y ffmpeg-free

#visual studio code
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
sudo dnf update -y
sudo dnf install -y code
sudo systemctl mask systemd-rfkill.service systemd-rfkill.socket

#audacity
sudo dnf install -y audacity

#steam
sudo dnf install -y steam

#Flatpak
sudo dnf install -y flatpak

#Redshift
sudo dnf install -y redshift
sudo dnf install -y redshift-gtk

#Discord
sudo dnf install -y discord

#File-Roller
sudo dnf install -y file-roller

#Increase cursor size
xfconf-query --channel xsettings --property /Gtk/CursorThemeSize --set 41

#arc-theme
sudo dnf install -y arc-theme

#papirus icon theme
sudo dnf install -y papirus-icon-theme

#xfce4 desktop config
wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/xfce4-desktop.xml

#xfce4 panel config
wget --content-disposition https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/eos-panel
xfce4-panel-profiles load eos-panel
rm eos-panel

#Matcha theme
sudo dnf install -y gtk-murrine-engine
sudo dnf install -y gtk2-engines
sudo dnf install -y git
git clone https://github.com/vinceliuice/matcha.git
cd matcha
bash install.sh
cd ~
sudo rm -r matcha

#Keyboard shortcuts
mkdir -p ~/.config/xfce4/xfconf/xfce-perchannel-xml/
wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/xfce4-keyboard-shortcuts.xml

#Terminal theme
mkdir -p ~/.config/xfce4/terminal/
wget -O ~/.config/xfce4/terminal/terminalrc https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/terminalrc

#Thunar Settings
wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/thunar.xml https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/thunar.xml

#Window scaling settings
wget -O ~/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/xsettings.xml

#Papirus + Matcha Dark Sea config
xfconf-query -c xfwm4 -p /general/theme -s "Matcha-dark-sea-xhdpi" --create -t string
xfconf-query -c xsettings -p /Net/ThemeName -s "Matcha-dark-sea" --create -t string
xfconf-query -c xsettings -p /Net/IconThemeName -s "Papirus-Dark" --create -t string

#dnf automatic
sudo dnf install -y dnf-automatic
sudo wget -O /etc/dnf/automatic.conf https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/automatic.conf
sudo systemctl enable --now dnf-automatic.timer

#resilio sync
sudo rpm --import https://linux-packages.resilio.com/resilio-sync/key.asc
sudo dnf install -y zypper
sudo zypper ar --gpgcheck-allow-unsigned-repo -f https://linux-packages.resilio.com/resilio-sync/rpm/\$basearch resilio-sync
printf "[resilio-sync]\nname=Resilio Sync\nbaseurl=https://linux-packages.resilio.com/resilio-sync/rpm/\$basearch\nenabled=1\ngpgcheck=1\n" | sudo tee /etc/yum.repos.d/resilio-sync.repo
sudo dnf update -y
sudo dnf install -y resilio-sync
sudo service resilio-sync stop
sudo systemctl disable resilio-sync
wget https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/resilio-sync.service
sudo mv resilio-sync.service /usr/lib/systemd/user/
systemctl --user enable resilio-sync
systemctl --user start resilio-sync

sudo dnf install -y xcb-util-cursor
sudo dnf install -y xcb-util-keysyms
sudo dnf install -y xcb-util-wm
sudo dnf install -y libxkbcommon-x11
export QT_QPA_PLATFORM=offscreen

#MPV
sudo dnf install -y mpv

#Auto-CPUFreq
cd ~
git clone https://github.com/AdnanHodzic/auto-cpufreq.git
cd auto-cpufreq
sudo ./auto-cpufreq-installer
cd ~

#Parsec
source /etc/profile.d/flatpak.sh
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak update --appstream && flatpak update -y
sudo flatpak install -y flathub com.parsecgaming.parsec

#zsh
wget -O .zshrc https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/.zshrc
sudo dnf install -y wget
sudo dnf install -y curl		
sudo dnf install -y zsh
sudo dnf install -y powerline-fonts
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
chsh -s $(which zsh)

