#!/bin/bash

#Update packages
sudo pacman-mirrors --geoip && sudo pacman -Syyu --noconfirm

#Nano
sudo pacman -S nano --noconfirm

#Yay
sudo pacman -S --needed git base-devel --noconfirm
sudo pacman -S git --noconfirm
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si --noconfirm
cd

#Printing Server
#sudo apt install -y cups printer-driver-escpr
#sudo pacman -S cups --noconfirm
#sudo pacman -S epson-inkjet-printer-escpr --noconfirm
#sudo systemctl start cups.service
#sudo cupsctl --remote-any
#sudo systemctl enable cups.service

#Neofetch
sudo pacman -S neofetch --noconfirm

#ZFS
#sudo apt install -y raspberrypi-kernel-headers
#sudo apt install -y zfs-dkms zfsutils-linux
sudo pacman -S base-devel dkms linux-rpi4-headers --noconfirm
#yay -S zfs --noconfirm
yay -S zfs-utils --noconfirm
yay -S zfs-dkms --noconfirm
sudo pacman -S wget --noconfirm
sudo wget -O /etc/systemd/system/zfs-import-zfspool.service https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/zfs-import-zfspool.service
sudo systemctl daemon-reload
sudo systemctl enable zfs-import-zfspool.service

#Resilio Sync
sudo pacman -S docker --noconfirm
sudo usermod -aG docker pi
newgrp docker
sudo systemctl enable docker
sudo systemctl start docker
docker run -d \
  --name=resilio-sync \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Etc/UTC \
  -p 8888:8888 \
  -p 55555:55555 \
  -v ~/.config/resilio-sync:/config \
  -v /zfspool/Downloads:/downloads \
  -v /zfspool/Sync:/sync \
  --restart unless-stopped \
  lscr.io/linuxserver/resilio-sync:latest

  docker run -d \
     --name watchtower \
     --restart always \
     -v /var/run/docker.sock:/var/run/docker.sock \
     containrrr/watchtower \
     --include-restarting

#Samba
sudo mkdir /zfspool/samba
sudo chown pi:pi /zfspool/samba
sudo pacman -S samba --noconfirm
sudo mkdir -p /etc/samba
sudo wget -O /etc/samba/smb.conf https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/smb.conf
sudo smbpasswd -a pi
sudo systemctl enable --now smb
sudo systemctl enable --now nmb

#zsh
#sudo apt install -y zsh
sudo pacman -S zsh --noconfirm
sudo pacman -S git --noconfirm
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
echo "if [ -f /usr/bin/screenfetch ]; then screenfetch -D "Raspbian"; fi" >> ~/.zshrc
echo "if [ ! -f /usr/bin/screenfetch ]; then neofetch; fi" >> ~/.zshrc
echo "/usr/bin/zpool list" >> ~/.zshrc
echo 'echo "\n"' >> ~/.zshrc
echo "/usr/bin/docker ps" >> ~/.zshrc
echo 'echo "\n"' >> ~/.zshrc

sed -i 's/robbyrussell/geoffgarside/g' ~/.zshrc


source .zshrc