#!/bin/bash

ls *.mkv 2> /dev/null
echo ""
echo "Desired of TV Show (eg. Star Wars The Clone Wars) "
read a
echo "Season Number (eg. S06) "
read n
echo "How many episodes? "
read p

x=*
i=1
c=1
while [ $i -lt 10 ]
do
        mv $x*"E0"$c*.mkv "$a "$n"E0"$c".mkv"
        ((c=c+1))
        ((i+=1))
done

while [ $i -gt 9 ] && [ "$i" -le "$p" ]
do
        mv $x*"E"$c*.mkv "$a "$n"E"$c".mkv"
        ((c=c+1))
        ((i+=1))
done
