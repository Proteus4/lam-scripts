#!/bin/bahs
#forge-server-1.16.3-34.1.35

while :; do
    clear
    echo ""
    echo ""
    echo "Forge Server Installer"
    echo ""
    echo "Please select an option"
    echo "1) Automatically install forge-server-1.16.3-34.1.35"
    echo "2) Install a different version of forge-server"
    echo "3) Download eula.txt"
    echo "99) Exit"

    read INPUT_STRING
    case $INPUT_STRING in

    1)
        clear
         if [[ $(whereis java | grep -o "java:") = "java:" ]]; then
            echo "Java Found! Continuing..."
         else
            echo "Java not Found! Do you want to install openjdk-8? [y/n]"
            read j
            if [[ $j == 'y' ]]; then
                sudo apt update
                sudo apt install -y openjdk-8-jdk
            fi
         fi
        mkdir forge-server
        cd forge-server
        wget --content-disposition https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.16.3-34.1.35/forge-1.16.3-34.1.35-installer.jar

        java -jar forge-*-installer.jar --installServer

        rm *installer*.jar

        wget --content-disposition https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/misc%20scripts/eula.txt

        echo ""
        echo "How much RAM (megabytes) to be allocated? (eg. 2048)"
        read r
        echo "java -Xmx"$r"M -Xms"$r"M -jar forge-*.jar nogui" > run.sh
        chmod +x run.sh
        rm ../forge-server-*.sh
        clear
        sh run.sh
        ;;

    2)
        clear
         if [[ $(whereis java | grep -o "java:") = "java:" ]]; then
            echo "Java Found! Continuing..."
         else
            echo "Java not Found! Do you want to install openjdk-8? [y/n]"
            read j
            if [[ $j == 'y' ]]; then
                sudo apt update
                sudo apt install -y openjdk-8-jdk
            fi
         fi
        mkdir forge-server
        cd forge-server
        echo "Would you like to visit minecraftforge.net? [y/n]"
        read w
        if [[ $w == 'y' ]]; then
        x-www-browser "http://files.minecraftforge.net/" 2> /dev/null &
        fi

        echo "Minecraft Version? (eg. 1.16.3)"
        read v
        echo "forge-server Version? (eg 34.1.0)"
        read f
        wget --content-disposition https://files.minecraftforge.net/maven/net/minecraftforge/forge/"$v"-"$f"/forge-"$v"-"$f"-installer.jar

                java -jar forge-*-installer.jar --installServer

        rm *installer*.jar

        wget --content-disposition https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/misc%20scripts/eula.txt

        echo ""
        echo "How much RAM (megabytes) to be allocated? (eg. 2048)"
        read r
        echo "java -Xmx"$r"M -Xms"$r"M -jar forge-*.jar nogui" > run.sh
        chmod +x run.sh
        rm ../forge-server-*.sh
        clear
        sh run.sh
        ;;

    3)
        wget --content-disposition https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/misc%20scripts/eula.txt
        ;;


    99)
        break;
        ;;

    *)
        echo "Invalid input. Try again."
        ;;
    esac
done
