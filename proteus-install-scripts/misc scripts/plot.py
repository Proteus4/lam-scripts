import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import wavfile
from moviepy.editor import *
from datetime import datetime
import getpass

def convert_mp3_to_wav(mp3_file, wav_file):
    # Run ffmpeg command to convert mp3 to wav
    os.system(f'ffmpeg -i "{mp3_file}" "{wav_file}"')

def plot_waveform(wav_file, color=None):
    rate, data = wavfile.read(wav_file)
    duration = len(data) / rate

    t = np.linspace(0, duration, len(data))
    plt.figure(figsize=(10, 4))
    
    if color is not None:
        plt.plot(t, data, color=color)
    else:
        plt.plot(t, data)

    plt.axis('off')
    plt.savefig('waveform.png', bbox_inches='tight')
    plt.close()

def create_video_with_waveform(wav_file, output_file, color=None):
    plot_waveform(wav_file, color)
    audio = AudioFileClip(wav_file)
    waveform_img = ImageClip('waveform.png', duration=audio.duration)

    # Set the frames per second (fps) for the video
    fps = 30

    video = waveform_img.set_audio(audio)
    video = video.set_duration(audio.duration)

    # Get current date and time
    current_datetime = datetime.now()
    formatted_datetime = current_datetime.strftime('%Y-%m-%d-%H-%M-%S')

    # Append formatted datetime to the output filename
    output_file = f"../../../Downloads/{formatted_datetime}.mp4"

    # Force audio bitrate to 512 kbps
    audio_bitrate = "512k"

    video.write_videofile(output_file, fps=fps, codec='libx264', audio_codec='aac', audio_bitrate=audio_bitrate)
    video.close()

if __name__ == "__main__":
    # Specify the directory where the files are located
    username = getpass.getuser()
    files_directory = f"C:/Users/{username}/Downloads/"

    # Prompt the user to enter the file name
    input_file = input("Enter the file name (e.g., AI_Test_Kitchen_fastpaced_drum__bass_strong_synth_piano.mp3): ")

    # Create the full path to the file
    full_path = os.path.join(files_directory, input_file)

    # Check if the file exists
    if not os.path.exists(full_path):
        print(f"File '{input_file}' not found in the specified directory.")
        exit()

    # Check if the file has an "mp3" extension
    if input_file.lower().endswith('.mp3'):
        # If it's an mp3 file, convert it to a temporary wav file and set color to green
        temp_wav_file = 'temp.wav'
        convert_mp3_to_wav(full_path, temp_wav_file)
        wav_file = temp_wav_file
        waveform_color = 'green'
    else:
        # Otherwise, assume it's a wav file
        wav_file = full_path
        waveform_color = 'green'
    # Set the default output file path
    output_file = "../../../Downloads/output_video_with_waveform.mp4"

    create_video_with_waveform(wav_file, output_file, waveform_color)

    # If a temporary wav file was created, remove it
    if 'temp_wav_file' in locals() and os.path.exists(temp_wav_file):
        os.remove(temp_wav_file)
