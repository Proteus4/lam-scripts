#!/bin/bash

sudo pacman -S archlinux-keyring --noconfirm
sudo pacman -Syyu --noconfirm
sudo pacman -S nano --noconfirm

#Yay
sudo pacman -S --needed git base-devel --noconfirm
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si --noconfirm
cd ~

#Wayland
sudo pacman -S --needed wayland --noconfirm

#SDDM Display Manager
yay -S sddm-git --noconfirm

#KDE Plasma Desktop
sudo pacman -S --needed xorg-xwayland xorg-xlsclients qt5-wayland glfw-wayland --noconfirm
sudo pacman -S --needed plasma kde-applications --noconfirm
sudo pacman -S --needed plasma--workspace --noconfirm

#Enable Managers
sudo systemctl enable sddm
sudo systemctl enable NetworkManager
sudo sed -i 's/^Current=.*/Current=breeze/' /usr/lib/sddm/sddm.conf.d/default.conf
sudo reboot now