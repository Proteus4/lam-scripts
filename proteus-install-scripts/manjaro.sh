#!/bin/bash
#https://bit.ly/3jvcU5R
clear
while :; do
echo ""
echo ""
echo "THIS SCRIPT IS FOR ARCH-BASED DISTRIBUTIONS."
echo "Please don't try to run this on Ubuntu."
echo ""
echo "Please select an option"
echo "1)  Remove unwanted applications"
echo "2)  Automated package install"
echo "3)  Perform options 1 & 2"
echo "99) Exit"
echo ""
read INPUT_STRING
case $INPUT_STRING in

    1)
        #Increase cursor size
        xfconf-query --channel xsettings --property /Gtk/CursorThemeSize --set 41;

        sudo wget --content-disposition https://bit.ly/3WTJaUQ -O /etc/pacman.conf;
        sudo pacman-mirrors --country Australia

        #Removing unwanted applications
        sudo pacman -Rs --noconfirm midori
        sudo pacman -Rs --noconfirm parole
        sudo pacman -Rs --noconfirm qpdfview

        echo ""
        echo "Task Completed!"
        echo "Returning to menu. . ."
    ;;

    2)
        #Increase cursor size
        xfconf-query --channel xsettings --property /Gtk/CursorThemeSize --set 41;

        sudo wget --content-disposition https://bit.ly/3WTJaUQ -O /etc/pacman.conf;
        sudo pacman-mirrors --country Australia

        sudo pacman -Syu --noconfirm

        #Audacity
        sudo pacman -S --noconfirm audacity

        #MPV
        sudo pacman -S --noconfirm mpv

        #Open VPN
        sudo pacman -S --noconfirm openvpn

        #Discord
        sudo pacman -S --noconfirm discord

        #FileZilla
        sudo pacman -S --noconfirm filezilla

        #Yay
        sudo pacman -S --noconfirm base-devel
        sudo pacman -S --noconfirm git
        cd ~
        git clone https://aur.archlinux.org/yay.git
        cd yay
        makepkg -si --noconfirm
        cd ~

        #Brave Beta
        yay -S --noconfirm brave-beta-bin --noconfirm

        echo ""
        echo "Task Completed!"
        echo "Returning to menu. . ."

        #xfce4 panel config
        yay -S --noconfirm xfce4-panel-profiles
        wget --content-disposition https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/mint-panel
        xfce4-panel-profiles load mint-panel;
        rm mint-panel;

        #TLP
        sudo pacman -S --noconfirm tlp
        sudo pacman -S --noconfirm tlp-rdw
        sudo systemctl enable tlp.service;

        #auto-cpufreq
        yay -S --noconfirm auto-cpufreq

        #zsh
        sudo apt install -y wget;
        sudo apt install -y curl;
        sudo apt install -y zsh;
        sudo apt install -y fonts-powerline;

        git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh;
        wget -O .zshrc https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/.zshrc;
        chsh -s /bin/zsh;

        cd ~
        git clone https://github.com/AdnanHodzic/auto-cpufreq.git
        cd auto-cpufreq
        sudo ./auto-cpufreq-installer

        xfce4-session-logout --fast --logout;
    ;;

    3)	
        #Increase cursor size
        xfconf-query --channel xsettings --property /Gtk/CursorThemeSize --set 41;

        sudo wget --content-disposition https://bit.ly/3WTJaUQ -O /etc/pacman.conf;
        sudo pacman-mirrors --country Australia

        #Removing unwanted applications
        sudo pacman -Syu --noconfirm
        sudo pacman -Rs --noconfirm midori
        sudo pacman -Rs --noconfirm parole
        sudo pacman -Rs --noconfirm qpdfview

        sudo pacman -Syu --noconfirm

        #TLP
        sudo pacman -S --noconfirm tlp
        sudo pacman -S --noconfirm tlp-rdw
        sudo systemctl enable tlp.service;

        #auto-cpufreq
        yay -S --noconfirm auto-cpufreq

        #Audacity
        sudo pacman -S --noconfirm audacity

        #MPV
        sudo pacman -S --noconfirm mpv

        #Open VPN
        sudo pacman -S --noconfirm openvpn

        #Discord
        sudo pacman -S --noconfirm discord

        #FileZilla
        sudo pacman -S --noconfirm filezilla

        #Yay
        sudo pacman -S --noconfirm base-devel
        sudo pacman -S --noconfirm git
        cd ~
        git clone https://aur.archlinux.org/yay.git
        cd yay
        makepkg -si --noconfirm
        cd ~

        #Brave Beta
        yay -S --noconfirm brave-beta-bin --noconfirm

        #xfce4 panel config
        yay -S --noconfirm xfce4-panel-profiles
        wget --content-disposition https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/mint-panel
        xfce4-panel-profiles load mint-panel;
        rm mint-panel;

        #zsh
        sudo apt install -y wget;
        sudo apt install -y curl;
        sudo apt install -y zsh;
        sudo apt install -y fonts-powerline;

        git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh;
        wget -O .zshrc https://gitlab.com/Proteus4/lam-scripts/-/raw/master/proteus-install-scripts/.zshrc;
        chsh -s /bin/zsh;

        cd ~
        git clone https://github.com/AdnanHodzic/auto-cpufreq.git
        cd auto-cpufreq
        sudo ./auto-cpufreq-installer

        xfce4-session-logout --fast --logout;

        #echo ""
        #echo "Task Completed!"
        #echo "There is nothing to do! Exiting. . ."
        #exit
    ;;

    99)
        break
    ;;

    *)
        clear
        echo "Invalid input. Try again."
    ;;
        esac
    done
