# Prerequisites
Linux Mint 20.04 'Ulyana' Xfce installed and rebooted

http://mirror.internode.on.net/pub/linuxmint/stable/20/linuxmint-20-xfce-64bit.iso


Insert a usb drive (at least 8 GB or higher capacity) and burn the iso file to the USB drive.

On Windows, Rufus is a utility that helps format and create bootable USB flash drives, such as USB keys/pendrives, memory sticks, etc.
https://rufus.ie/.

It should look something like this:

![](https://i.imgur.com/qkD4MxA.png)


Once Rufus is finished, eject the usb drive and insert it into the computer you wish to install it on. Reboot the target computer and boot from usb.


# Installation
## You may have to enter your password multiple times during the installation.
Once you have sucessfully installed Linux Mint and rebooted, download and run the script.
```
wget --content-disposition https://bit.ly/3aCMfRN
bash seido.sh
```


# Cleanup
```
rm seido.sh
```