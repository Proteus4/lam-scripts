#!/bin/bash
#https://bit.ly/3aCMfRN
while :; do
        clear
        echo ""
        echo ""
        echo "Select Option"
        echo "1) Full setup"
        echo "2) Switch to a local mirror"
        echo "3) Install themes and configs"
        echo "99) Exit"

        read INPUT_STRING
        case $INPUT_STRING in

        1)
        wget --content-disposition https://bit.ly/2Fprhdy;
        sudo cp -r official-package-repositories.list /etc/apt/sources.list.d/;
        sudo rm official-package-repositories.list;
        sudo apt update;
        sudo apt upgrade -y;

        #discord
        wget -O discord.deb "https://discordapp.com/api/download?platform=linux&format=deb";
        sudo dpkg -i discord.deb;
        sudo apt install -f -y;
        rm discord.deb;

        #vlc
        sudo apt remove -y --purge celluloid;
        sudo apt install -y vlc;

        #screenfetch
        sudo apt install -y screenfetch;

        #ffmpeg
        sudo apt install -y ffmpeg;

        #vscodium
        echo "deb [signed-by=/etc/apt/trusted.gpg.d/vscodium-archive-keyring.gpg] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo debs/ vscodium main" | sudo tee -a /etc/apt/sources.list.d/vscodium.list;
        wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium-archive-keyring.gpg;
        sudo apt update;
        sudo apt install -y vscodium;
        
        #teamviewer
        wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb;
        sudo apt install -y ./teamviewer_amd64.deb;
        rm teamviewer_amd64.deb;

        #steam
        sudo apt install -y steam;
        
        #tlp
        sudo add-apt-repository -y ppa:linrunner/tlp
        sudo apt install -y tlp;   

        #Google Chrome Stable
        sudo apt remove -y --purge firefox;
        sudo apt remove -y --purge firefox-locale-en;
        wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb;
        sudo dpkg -i google-chrome-stable_current_amd64.deb; sudo apt-get -f install;
        rm google-chrome-stable_current_amd64.deb;

        #vmware
        sudo apt install build-essential -y;
        wget https://download3.vmware.com/software/wkst/file/VMware-Workstation-Full-15.5.6-16341506.x86_64.bundle;
        sudo bash VMware-Workstation-Full*.bundle;
        rm VMware-Workstation-Full*.bundle;

        sudo apt update;
        sudo apt upgrade -y;
    
        #chicago 95
        sudo apt install -y git;
        git clone https://github.com/grassmunk/Chicago95.git;
        mkdir -p ~/.themes;
        mkdir -p ~/.icons;
        cp -r Chicago95/Theme/Chicago95 ~/.themes;
        cp -r Chicago95/Icons/* ~/.icons;
        sudo rm -r Chicago95;

        #arc-theme
        sudo apt install -y arc-theme

        #papirus icon theme
        sudo apt install -y papirus-icon-theme;

        #Papirus + Arc Dark Sea config
        xfconf-query -c xfwm4 -p /general/theme -s "Arc-Dark"
        xfconf-query -c xsettings -p /Net/ThemeName -s "Arc-Dark"
        xfconf-query -c xsettings -p /Net/IconThemeName -s "Papirus-Dark"

        #zsh
        sudo apt install -y wget;
        sudo apt install -y curl;
        sudo apt install -y zsh;
        sudo apt install -y fonts-powerline;
        sudo apt install -y xterm;
        cd ~;
        git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh;
        wget -O .zshrc https://bit.ly/2Q0ZzG1;
        chsh -s /bin/zsh;

        sudo git clone https://github.com/robbyrussell/oh-my-zsh.git /root/.oh-my-zsh;
        sudo wget -O /root/.zshrc https://bit.ly/3av00C0
        sudo xterm -e 'chsh -s /bin/zsh;' &
        xfce4-session-logout --fast --logout;
        ;;

        2)
        wget --content-disposition https://bit.ly/2Fprhdy
        sudo cp -r official-package-repositories.list /etc/apt/sources.list.d/;
        sudo rm official-package-repositories.list;
        sudo apt update;
        ;;

        3)
        sudo apt update
        sudo apt upgrade -y
    
        #chicago 95
        sudo apt install -y git;
        git clone https://github.com/grassmunk/Chicago95.git;
        mkdir -p ~/.themes;
        mkdir -p ~/.icons;
        cp -r Chicago95/Theme/Chicago95 ~/.themes;
        cp -r Chicago95/Icons/* ~/.icons;
        sudo rm -r Chicago95;

        #arc-theme
        sudo apt install -y arc-theme

        #papirus icon theme
        sudo apt install -y papirus-icon-theme;

        #Papirus + Arc Dark Sea config
        xfconf-query -c xfwm4 -p /general/theme -s "Arc-Dark"
        xfconf-query -c xsettings -p /Net/ThemeName -s "Arc-Dark"
        xfconf-query -c xsettings -p /Net/IconThemeName -s "Papirus-Dark"

        #zsh
        sudo apt install -y wget;
        sudo apt install -y curl;
        sudo apt install -y zsh;
        sudo apt install -y fonts-powerline;
        sudo apt install -y xterm;
        cd ~;
        git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh;
        wget -O .zshrc https://bit.ly/2Q0ZzG1;
        chsh -s /bin/zsh;

        sudo git clone https://github.com/robbyrussell/oh-my-zsh.git /root/.oh-my-zsh;
        sudo wget -O /root/.zshrc https://bit.ly/3av00C0
        sudo xterm -e 'chsh -s /bin/zsh;' &
        xfce4-session-logout --fast --logout;
        ;;

        99)
        break;
        ;;

        *)
        echo Invalid Input!
        ;;
    esac
done